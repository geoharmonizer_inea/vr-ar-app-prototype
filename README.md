# Geo-harmonizer Virtual/Augmented Reality Applications

[Virtual reality](https://en.wikipedia.org/wiki/Virtual_reality) (VR)
 and [augmented
 reality](https://en.wikipedia.org/wiki/Augmented_reality) (AR) are
 nowadays very popular and used technologies in many fields,
 unfortunately GIS data visualization is not this case.  This part of
 [Geo-harmonizer
 project](https://opendatascience.eu/geoharmonizer-project/)
 focuses on testing various workflows how to visualize the GIS data
 with this two technologies. Final products are two prototype applications for
 each technology. VR and AR giving new perspective how the GIS outputs
 could look like.

Source code in this repository is licensed under an
[Apache-2](LICENSE) license.

## Sample applications

### AR application

AR application was created with the [Swift programming
language](https://en.wikipedia.org/wiki/Swift_programming_language) programming language and [Xcode](https://en.wikipedia.org/wiki/Xcode). 
For most user interfaces the SwiftUI Framework was used. Other elements were created by UIKit. For AR technology the [RealityKit](https://developer.apple.com/documentation/realitykit) and [ARKit](https://developer.apple.com/documentation/arkit) frameworks were used. Application is designed for iPads with a 12.9 inch display, for smaller displays it is not optimal. Application supports iOS 12.1 and higher.

Source code of AR application is located in [ar
directory](ar).

<img src=img/ar.png width=650/>

_Fig: AR application prototype_

[![](http://img.youtube.com/vi/ORHV_CoVHkk/0.jpg)](http://www.youtube.com/watch?v=ORHV_CoVHkk "Video: Geo-harmonizer AR Application Prototype Demonstration")

[Video: AR Application Prototype Demonstration](http://www.youtube.com/watch?v=ORHV_CoVHkk)

Sample application can be downloaded from
[App Store](https://apps.apple.com/us/app/opendatascience-ar/id1599107416)

## VR application

Presented VR application created in [Unreal Engine](https://en.wikipedia.org/wiki/Unreal_Engine) 4 and it is
primarily designed for VR glasses HTC VIVE PRO.

Sample VR application contains Czech republic and Netherland data
sets. The region was split into 30x30 km tiles. The DTM model is
provided 3D glTF format. Land cover dataset is provided as PNG
files. Spatial resolution is 30m.

Sample application can be download in [7z
archive](https://geo.fsv.cvut.cz/geoharmonizer/vr/Geo-harmonizer_sample_VR_app.7z)
(2.6GB)

[![](http://img.youtube.com/vi/dUbnxjWg6I4/0.jpg)](http://www.youtube.com/watch?v=dUbnxjWg6I4 "Video: Geo-harmonizer VR Application Prototype Demonstration")

[Video: VR Application Prototype Demonstration](http://www.youtube.com/watch?v=dUbnxjWg6I4)

## Create your own VR application

This cookbook demostrates how to create your own VR application.

## 1. Data preparation

Splitting country into tiles give us option to select specific area of
interest. It is also perfect for smooth visualization; in case than it
is used DTM of whole country with pixel size 30x30m as used for tiles;
it would be too difficult for hardware and world could lag.

See [STEP-BY-STEP INSTRUCTIONS](data-preparation/README.md) how to
prepare input data for VR application purposes.

Best and easiest way is to load glTF files created in data preparation
phase directly to Unreal Engine 4 via plugin glTF importer. If Unreal
Engine has problem with created data set and crashes or does not load
data properly use FBX files. You can use free software Blender for
data convertion from glTF to FBX. The qgis2threejs QGIS plugin which
used for creating glTF tiles works perfectly. All prepared data can be
loaded to Unreal Engine.

## 2. Unreal Engine

Project is situated in the building created in the Unreal Engine. This
building has 5 basic stations where models are appeared.  Let’s
introduce these stations in a virtual world and settings in Unreal for
better orientation. First station is called _Visualization_, second
station is called _Comparison_, third is called _Overview_, fourth is
called _Main Menu_ and last fifth station is called _Animation_ (DEMO
station).

Complex project for Unreal Engine can be download in [7z
archive](https://geo.fsv.cvut.cz/geoharmonizer/vr/Geo-harmonizer_unreal_engine_project.7z)
(3.1GB)

Every stage has different functions because integration of all
functions together to one the station will be confusing.

<img src=img/GeoHarmonizer.png width=650/>

_Fig: Room stations_

Geo-harmonizer project for Unreal contains all widgets and blueprints
that were used for main application and could be used as well for any
other project. All widgets are designed simply so that every user can
customize them according their needs. It is not necessary to describe
all of the implemented functions because user can find all of this
blueprints, materials and connection in Geo-harmonizer project. Here
is an overview and description of all [Blueprint
Class](https://docs.unrealengine.com/4.27/en-US/ProgrammingAndScripting/Blueprints/UserGuide/Types/ClassBlueprint/),
[Widget
Blueprints](https://docs.unrealengine.com/4.27/en-US/InteractiveExperiences/UMG/UserGuide/WidgetBlueprints/)
and
[Materials](https://docs.unrealengine.com/4.27/en-US/RenderingAndGraphics/Materials/).

[![](http://img.youtube.com/vi/MIUVtgU_Se8/0.jpg)](http://www.youtube.com/watch?v=MIUVtgU_Se8 "Video: VR Tutorial - part 1 (room stations)")

[Video: VR Tutorial - part 1 (room stations)](http://www.youtube.com/watch?v=MIUVtgU_Se8)

### Main Menu – tile selection

Widget Blueprints: _MainMenuMapWidgetArray_ – A map with active buttons for every
tile. This widget controls spawn of scenes on set locations. Functions
are collapsed into simple graph that can be easily edited. Contains
the coordinates where tile will spawn on all three x, y, z axes.

Blueprint class: _BP\_MainMenuMap_ – Blueprint that contain _MainMenuMapWidgetArray_.

[![](http://img.youtube.com/vi/5r67uNH_9Gw/0.jpg)](http://www.youtube.com/watch?v=5r67uNH_9Gw "Video: VR Tutorial - part 2 (main menu)")

[Video: VR Tutorial - part 2 (main menu)](http://www.youtube.com/watch?v=5r67uNH_9Gw)

### Content of every tile folder

Widget Blueprints: LandCover Slider – _LCslider#tilenumber.widget_ – Widget contains two
sliders, first slider _Map visibility/Opacity_ level controls visibility
of a map as a primary texture. By this slider it is easy to set
opacity level that user wants, second texture are _Land covers textures_
so the opacity level always uses primary map texture. Second slider is
_LandCoverSelect_. This slider controls year of land cover and connects
it with the first slider.

Every tile folder contains one more slider widget -
_LCslider#tilenumber.widget2_. It is copy of the first slider. It is
used at a comparison stage. In this widget there are two primarily
variables.

YearRange – year range of slider

LandCovers array – it is array that contains whole 20 years of land cover.

Blueprints class: All blueprints classes connect widgets, 3D DTM
models and functions also contain spawn position for every stage. This
blueprints calling widgets and connect functions as material change on
static meshes. Here is also place for setting up the scale for your
models.

_BP\_numberOfTile\_pos1_ - visualization

_BP\_numberOfTile\_pos2_ – comparison

_BP\_numberOfTile\_pos3_ - overview

Material - _tilenumber.mat_ – Material with alpha channel that work
with opacity level. It contains one texture with map – base texture
and second that will be changed by second slider.

[![](http://img.youtube.com/vi/wH0r9hiU-ew/0.jpg)](http://www.youtube.com/watch?v=wH0r9hiU-ew "Video: VR Tutorial - part 1 (content settings)")

[Video: VR Tutorial - part 3 (content settings)](http://www.youtube.com/watch?v=wH0r9hiU-ew)

### Used plug-ins for Unreal Engine 4

- [Free Furniture
Pack](https://www.unrealengine.com/marketplace/en-US/product/a4907129f69c44a892f76782489736ab)
by Next Level 3D
- [temperate Vegetation: Spruce
Forest](https://www.unrealengine.com/marketplace/en-US/product/interactive-spruce-forest)
by Project Nature

## Funding

This work is co-financed under Grant Agreement [Connecting Europe
Facility (CEF) Telecom project
2018-EU-IA-0095](https://ec.europa.eu/inea/en/connecting-europe-facility/cef-telecom/2018-eu-ia-0095)
by the European Union.

<img src=img/CEF_programme_logo_650px.png width=650/>
