//
//  ARDataModel.swift
//  GISARDemo
//
//  Created by Jakub Jurnik on 23/04/2021.
//

import Combine
import RealityKit
import ARKit

final class ARDataModel: ObservableObject {
    //static var shared = ARDataModel()
    @Published var arView: MapARView!
    @Published var selectedTile: Int
    @Published var modelNumbers = [
        [00000, 00000, 00000, 00000, 00000, 12155, 12156, 12157, 00000, 00000, 00000, 00000, 00000, 00000, 00000, 00000,00000, 00000],
        [00000, 00000, 00000, 00000, 12342, 12343, 12344, 12345, 12346, 12347, 12348, 12349, 12350, 12351, 00000, 00000, 00000, 00000],
        [00000, 00000, 00000, 12529, 12530, 12531, 12532, 12533, 12534, 12535, 12536, 12537, 12538, 12539, 12540, 12541, 00000, 00000],
        [00000, 00000, 12716, 12717, 12718, 12719, 12720, 12721, 12722, 12723, 12724, 12725, 12726, 12727, 12728, 12729, 00000, 00000],
        [00000, 12903, 12904, 12905, 12906, 12907, 12908, 12909, 12910, 12911, 12912, 12913, 12914, 12915, 12916, 12917, 12918, 12919],
        [00000, 13091, 13092, 13093, 13094, 13095, 13096, 13097, 13098, 13099, 13100, 13101, 13102, 13103, 13104, 13105, 13106, 13107],
        [13278, 13279, 13280, 13281, 13282, 13283, 13284, 13285, 13286, 13287, 13288, 13289, 13290, 13291, 13292, 13293, 13294, 00000],
        [00000, 13467, 13468, 13469, 13470, 13471, 13472, 13473, 13474, 13475, 13476, 13477, 13478, 13479, 13480, 00000, 00000, 00000],
        [00000, 00000, 00000, 13657, 13658, 13659, 13660, 13661, 13662, 13663, 13664, 13665, 00000, 00000, 00000, 00000, 00000, 00000],
        [00000, 00000, 00000, 00000, 00000, 13847, 13848, 13849, 13850, 00000, 00000, 00000, 00000, 00000, 00000, 00000, 00000, 00000]
    ]
    
    @Published var isLoadingFiles: Bool
    
    init() {
        selectedTile = 0
        isLoadingFiles = false
        arView = MapARView(frame: .zero)
        let config = ARWorldTrackingConfiguration()
        config.planeDetection = [.horizontal]
        config.environmentTexturing = .automatic
        
        arView.session.run(config, options: [])
        
        arView.addCoaching()
        arView.setupGestures()
        arView.session.delegate = arView
    }
    
    func setSelectedTile(value: Int) {
        self.selectedTile = value
    }
    
    func toggleIsLoadingFiles() {
        self.isLoadingFiles = !self.isLoadingFiles
    }
}

