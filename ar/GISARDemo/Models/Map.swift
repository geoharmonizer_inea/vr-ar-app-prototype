//
//  Map.swift
//  GISARDemo
//
//  Created by Jakub Jurnik on 15/04/2021.
//

import SwiftUI

struct Map: Identifiable, Codable {
    let id: UUID
    var name: String
    var tileNumber: Int
    
    init(id: UUID = UUID(), name: String, tileNumber: Int) {
        self.id = id
        self.name = name
        self.tileNumber = tileNumber
    }
}

extension Map {
    static var tiles: [Map] {
        [
            Map(name: "map12155", tileNumber: 12155),
            Map(name: "map12156", tileNumber: 12156),
            Map(name: "map12157", tileNumber: 12157),
        ]
    }
}
