//
//  LoadingView.swift
//  GISARDemo
//
//  Created by Jakub Jurnik on 03.11.2021.
//

import SwiftUI

struct LoadingView<Content>: View where Content: View {

    var isShowing: Bool
    var message: String
    var content: () -> Content

    var body: some View {
        GeometryReader { geometry in
            ZStack(alignment: .center) {

                self.content()
                    .disabled(self.isShowing)
                    .blur(radius: self.isShowing ? 3 : 0)

                VStack {
                    Text(message)
                    ProgressView().progressViewStyle(CircularProgressViewStyle())
                }
                .frame(width: geometry.size.width / 2,
                       height: geometry.size.height / 5)
                .background(Color.secondary.colorInvert())
                .foregroundColor(Color.primary)
                .cornerRadius(20)
                .opacity(self.isShowing ? 1 : 0)

            }
        }
    }

}
