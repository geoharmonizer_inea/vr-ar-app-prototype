//
//  MapGridModel.swift
//  GISARDemo
//
//  Created by Jakub Jurnik on 13.10.2021.
//

import Foundation

class MapGrid: ObservableObject {
    static var modelNumbers = [[12155...12157], [12342...12351], [12529...12541], [12716...12729], [12903...12919], [13091...13107], [13278...13294], [13467...13480], [13657...13665], [13847...13850]]
    
    static var verticalStep = 188
    @Published var selectedTile: Int
    @Published var mapPlaced: Bool
    
    init() {
        self.selectedTile = 0
        self.mapPlaced = false
    }
    
    func setSelectedTile(value: Int) {
        self.selectedTile = value
    }
    
    func toggleMapPlaced() {
        self.mapPlaced = !self.mapPlaced
    }
}

