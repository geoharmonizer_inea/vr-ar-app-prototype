//
//  MainView.swift
//  GISARDemo
//
//  Created by Jakub Jurnik on 15/04/2021.
//

import SwiftUI

struct MainView: View {
    @EnvironmentObject var arDataModel: ARDataModel
    @State var menuVisible = false
    @State var deleteModeOn = false
    @State var infoModalVisible = false
    @State var downloadedTileArray = ARDataModel().arView.mainViewDownloadedTiles(tiles: ARDataModel().modelNumbers)//[[Bool]]
    let networkReachability = NetworkReachability()
    
    var body: some View {
        ModalView(isShowing: $infoModalVisible, message: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.") {
            ZStack {
                ZoomableScrollView {
                    ZStack {
                        Image("CZ_mainMenu")
                            .resizable()
                            .scaledToFit()
                        VStack(spacing: 0){
                            ForEach(0..<10) { vIndex in
                                HStack(spacing: 0){
                                    ForEach(0..<arDataModel.modelNumbers[9-vIndex].count) { hIndex in
                                        if arDataModel.modelNumbers[9-vIndex][hIndex] == 0 {
                                            Text("")
                                            .buttonStyle(PlainButtonStyle())
                                            .frame(width: 62.5, height: 62.5)
                                        } else {
                                            if deleteModeOn {
                                                Button(action: {
                                                    //print("deleting tile: \(arDataModel.modelNumbers[9-vIndex][hIndex])")
                                                    arDataModel.arView.removeTileFiles(tileNumber: arDataModel.modelNumbers[9-vIndex][hIndex])
                                                    downloadedTileArray[9-vIndex][hIndex] = false
                                                }) {
                                                    MapDeleteButton(isDownloaded: downloadedTileArray[9-vIndex][hIndex])
                                                }
                                                .frame(width: 62.5, height: 62.5)
                                                //.border(Color.red, width: 0.1)
                                            } else {
                                                if networkReachability.reachable || downloadedTileArray[9-vIndex][hIndex] {
                                                    NavigationLink(destination: ARDisplayView(selectedTile: arDataModel.modelNumbers[9-vIndex][hIndex])){
                                                        MapButton(isDownloaded: downloadedTileArray[9-vIndex][hIndex])
                                                        .frame(width: 62.5, height: 62.5)
                                                        //.border(Color.red, width: 0.1)
                                                    }
                                                } else {
                                                    Text("")
                                                    .buttonStyle(PlainButtonStyle())
                                                    .frame(width: 62.5, height: 62.5)
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }.offset(x: -21.5, y: -6.5)
                    }
                }.onAppear {
                    downloadedTileArray = arDataModel.arView.mainViewDownloadedTiles(tiles: arDataModel.modelNumbers)
                }
                if menuVisible {
                    MainMenu(menuVisible: $menuVisible, deleteModeOn: $deleteModeOn, infoModalVisible: $infoModalVisible)
                } else {
                    MainMenuButton(menuVisible: $menuVisible)
                }
                if deleteModeOn {
                    VStack {
                        Spacer()
                        HStack {
                            Button(action: {
                                deleteModeOn = false
                            }) {
                                HStack(spacing: 0){
                                    Text("Finish Deleting")
                                }.padding().background(Color.white)
                            }
                        }
                    }
                }
            }
        }
    }
}
