//
//  MapARView.swift
//  GISARDemo
//
//  Created by Jakub Jurnik on 15/04/2021.
//

import RealityKit
import ARKit
import SwiftUI
import Combine

class MapARView: ARView, ARSessionDelegate {
    var tileArray: [[Int]] = []
    let coachingOverlay = ARCoachingOverlayView()
    var mapPlaced = false
    var selectedTile = 0
    
}
