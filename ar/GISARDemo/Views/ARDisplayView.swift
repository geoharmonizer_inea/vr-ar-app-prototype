//
//  ARDisplayView.swift
//  GISARDemo
//
//  Created by Jakub Jurnik on 14/04/2021.
//

import SwiftUI
import RealityKit
import ARKit

struct ARDisplayView: View {
    @State private var selectedLayer = 2000.0
    @State private var layerOpacity = 0.0
    @Environment(\.presentationMode) var presentationMode
    @EnvironmentObject var arDataModel: ARDataModel
    var selectedTile: Int
    @State var legendVisible = false
    
    @State var isAnimationRunning = false
    @State var timer: Timer? = nil
    
    var body: some View {
        LoadingView(isShowing: arDataModel.isLoadingFiles, message: "Downloading and preparing files...") {
            ZStack {
                ARViewContainer(selectedTile: selectedTile, layer: selectedLayer, opacity: layerOpacity).edgesIgnoringSafeArea(.all)
                VStack {
                    Slider(value: $layerOpacity, in: 0...1, step: 0.25)
                        .padding([.horizontal, .top])
                        .onChange(of: layerOpacity, perform: { value in
                            arDataModel.arView.handleLayerChange(layer: selectedLayer, opacity: layerOpacity)
                        })
                    HStack {
                        Button(action: {
                            DispatchQueue.main.async {
                                arDataModel.arView.removeMap()
                                self.presentationMode.wrappedValue.dismiss()
                            }
                        }){
                            Image(systemName: "arrowshape.turn.up.backward.fill")
                                .frame(width: 40, height: 40)
                                .foregroundColor(.white)
                        }.padding(.horizontal)
                        Spacer()
                    }
                    Spacer()
                    HStack {
                        if legendVisible {
                            MapLegend()
                            Button(action: {legendVisible = false}){
                                Image(systemName: "chevron.left")
                                    .frame(width: 40, height: 40)
                                    .foregroundColor(.white)
                            }
                        } else {
                            Button(action: {legendVisible = true}){
                                Image(systemName: "chevron.right")
                                    .frame(width: 40, height: 40)
                                    .foregroundColor(.white)
                            }
                        }
                        Spacer()
                    }
                    Spacer()
                    HStack {
                        Spacer()
                        Text(selectedLayer.clean)
                            .foregroundColor(.black)
                            .padding([.leading,.trailing])
                            .background(Color.white)
                            .opacity(0.5)
                        HStack {
                            if isAnimationRunning {
                                Button(action: {stopAnimation()}){
                                    Image(systemName: "pause.fill")
                                        .frame(width: 30, height: 30)
                                        .foregroundColor(.white)
                                }
                            } else {
                                Button(action: {startAnimation()}){
                                    Image(systemName: "play.fill")
                                        .frame(width: 30, height: 30)
                                        .foregroundColor(.white)
                                }
                            }
                        }
                        Spacer()
                    }
                    Slider(value: $selectedLayer, in: 2000...2019, step: 1.0)
                        .padding([.horizontal, .bottom])
                        .onChange(of: selectedLayer, perform: { value in
                            arDataModel.arView.handleLayerChange(layer: selectedLayer, opacity: layerOpacity)
                        })
                }
                .navigationBarBackButtonHidden(true)
                .navigationBarTitle("")
                .navigationBarHidden(true)
                }
            }
    }
    
    func startAnimation() {
        isAnimationRunning = true
        if selectedLayer == 2019.0 {
            selectedLayer = 2000.0
        }
        timer = Timer.scheduledTimer(withTimeInterval: 2, repeats: true) { tempTimer in
            if selectedLayer < 2019.0 {
                selectedLayer = selectedLayer + 1
            } else {
                stopAnimation()
            }
            
        }
    }
    
    func stopAnimation() {
        isAnimationRunning = false
        timer?.invalidate()
        timer = nil
    }
}

struct ARViewContainer: UIViewRepresentable {
    @EnvironmentObject var arDataModel: ARDataModel
    var selectedTile: Int
    var layer: Double
    var opacity: Double
    func makeUIView(context: Context) -> MapARView {
        arDataModel.arView.tileArray = arDataModel.modelNumbers
        if !arDataModel.arView.mapFilesDownloaded(selectedTile: selectedTile) {
            arDataModel.isLoadingFiles = true
            DispatchQueue.main.async {
                print(arDataModel.isLoadingFiles)
                arDataModel.arView.downloadTextureFiles(tileNumber: selectedTile)
                arDataModel.arView.downloadModel(tileNumber: selectedTile)
                arDataModel.isLoadingFiles = false
                arDataModel.arView.handleTileChange(selectedTile: selectedTile, layer: layer, opacity: opacity)
            }
            
        } else {
            arDataModel.arView.handleTileChange(selectedTile: selectedTile, layer: layer, opacity: opacity)
        }
        return arDataModel.arView
    }
    
    func updateUIView(_ uiView: MapARView, context: Context) {
    }
}

struct ARDisplayView_Previews: PreviewProvider {
    static var previews: some View {
        ARDisplayView(selectedTile: 0)
    }
}
