//
//  ModalView.swift
//  GISARDemo
//
//  Created by Jakub Jurnik on 07.12.2021.
//


import SwiftUI

struct ModalView<Content>: View where Content: View {

    @Binding var isShowing: Bool
    var message: String
    var content: () -> Content

    var body: some View {
        GeometryReader { geometry in
            ZStack(alignment: .center) {

                self.content()
                    .disabled(self.isShowing)
                    .blur(radius: self.isShowing ? 3 : 0)
                HStack {
                    Spacer()
                    VStack {
                        HStack {
                            Spacer()
                            Button(action: {
                                isShowing = false
                            }) {
                                HStack(spacing: 0){
                                    Image(systemName: "multiply.circle")
                                       .resizable()
                                       .frame(width: 20, height: 20)
                                       .font(.largeTitle)
                                       .padding()
                                       .foregroundColor(.black)
                                }
                            }
                        }
                        Text(message).foregroundColor(Color.black).padding()
                    }
                    .frame(width: geometry.size.width / 2)
                    .background(Color.white)
                    .foregroundColor(Color.primary)
                    .cornerRadius(20)
                    .opacity(self.isShowing ? 1 : 0)
                    Spacer()
                }
            }
        }
    }

}
