//
//  MapButton.swift
//  GISARDemo
//
//  Created by Jakub Jurnik on 03.11.2021.
//

import SwiftUI

struct MapButtonStyle: ButtonStyle {
    public func makeBody(configuration: Configuration) -> some View {
        configuration.label
            .background(Color.blue)
            .foregroundColor(.white)
    }
}

struct MapButton: View {
    var isDownloaded: Bool
    
    var body: some View{
            VStack{
                HStack{
                    Spacer()
                    if isDownloaded {
                        Image(systemName: "checkmark.circle").foregroundColor(.green)
                    }
                }
                Spacer()
            }
        }
}

struct MapDeleteButton: View {
    var isDownloaded: Bool
    
    var body: some View{
            if isDownloaded {
                Image(systemName: "trash")
                   .resizable()
                   .frame(width: 30, height: 30)
                   .font(.largeTitle)
                   .padding()
                   .foregroundColor(.red)
            }
        }
}
