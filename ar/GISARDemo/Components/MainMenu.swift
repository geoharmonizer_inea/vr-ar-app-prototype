//
//  MainMenu.swift
//  GISARDemo
//
//  Created by Jakub Jurnik on 30.11.2021.
//

import SwiftUI

struct MainMenu: View {
    @EnvironmentObject var arDataModel: ARDataModel
    @Binding var menuVisible: Bool
    @Binding var deleteModeOn: Bool
    @Binding var infoModalVisible: Bool
    
    var body: some View {
        VStack{
            HStack{
                VStack(spacing: 0) {
                    VStack(alignment: .leading) {
                        Button(action: {
                            menuVisible = false
                        }) {
                            Image(systemName: "line.3.horizontal")
                                .resizable()
                                .frame(width: 30, height: 30)
                                .font(.largeTitle)
                                .padding()
                                .foregroundColor(.black)
                        }
                        //Spacer()
                        Button(action: {
                            deleteModeOn = true
                        }) {
                            HStack(spacing: 0){
                                Image(systemName: "trash.square")
                                   .resizable()
                                   .frame(width: 30, height: 30)
                                   .font(.largeTitle)
                                   .padding()
                                   .foregroundColor(.black)
                                Text("Deleta individual tiles")
                            }.padding([.horizontal])
                        }
                        Button(action: {
                            arDataModel.arView.removeDownloadedFiles()
                        }) {
                            HStack(spacing: 0){
                                Image(systemName: "trash")
                                   .resizable()
                                   .frame(width: 30, height: 30)
                                   .font(.largeTitle)
                                   .padding()
                                   .foregroundColor(.black)
                                Text("Deleta all tiles")
                            }.padding([.horizontal])
                        }
                        Button(action: {
                            infoModalVisible = true
                            menuVisible = false
                        }) {
                            HStack(spacing: 0){
                                Image(systemName: "info.circle")
                                   .resizable()
                                   .frame(width: 30, height: 30)
                                   .font(.largeTitle)
                                   .padding()
                                   .foregroundColor(.black)
                                Text("About")
                            }.padding([.horizontal])
                        }
                    }.background(Color.white)
                    Spacer()
                }
                //.frame(maxHeight: .infinity)
                Spacer()
            }
        }
    }
}
