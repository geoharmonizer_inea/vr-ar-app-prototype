//
//  MainMenuButton.swift
//  GISARDemo
//
//  Created by Jakub Jurnik on 30.11.2021.
//

import SwiftUI

struct MainMenuButton: View {
    @Binding var menuVisible: Bool
    @Environment(\.colorScheme) var colorScheme
    
    var body: some View {
        VStack {
            HStack {
                Button(action: {
                    menuVisible = true
                }) {
                    Image(systemName: "line.3.horizontal")
                       .resizable()
                       .frame(width: 30, height: 30)
                       .font(.largeTitle)
                       .padding()
                       .foregroundColor(colorScheme == .dark ? Color.white : Color.black)
                }
                Spacer()
            }
            Spacer()
        }
    }
}
