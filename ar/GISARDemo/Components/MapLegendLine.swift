//
//  MapLegendLine.swift
//  GISARDemo
//
//  Created by Jakub Jurnik on 29.11.2021.
//

import SwiftUI

struct MapLegendLine: View {
    var color: Color
    var label: String
    var padding: Edge.Set
    
    var body: some View {
        HStack{
            Text("").frame(width: 10, height: 10).background(color)
            Text(label).foregroundColor(Color.black)
        }.padding(padding)
    }
}
