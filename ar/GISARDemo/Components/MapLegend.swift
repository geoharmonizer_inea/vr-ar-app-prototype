//
//  MapLegend.swift
//  GISARDemo
//
//  Created by Jakub Jurnik on 09.11.2021.
//

import SwiftUI

struct MapLegend: View {
    var body: some View{
        ScrollView {
            VStack(alignment: .leading){
                Group {
                    MapLegendLine(color: Color(red: 1, green: 0, blue: 0), label: "111-Urban fabric", padding: [.horizontal, .top])
                    MapLegendLine(color: Color(red: 0.8, green: 0, blue: 0), label: "122-Road and rali networks and associated land", padding: [.horizontal])
                    MapLegendLine(color: Color(red: 0.902, green: 0.8, blue: 0.8), label: "123-Port areas", padding: [.horizontal])
                    MapLegendLine(color: Color(red: 0.902, green: 0.8, blue: 0.902), label: "124-Airports", padding: [.horizontal])
                    MapLegendLine(color: Color(red: 0.651, green: 0, blue: 0.8), label: "131-Mineral extraction sites", padding: [.horizontal])
                    MapLegendLine(color: Color(red: 0.651, green: 0.302, blue: 0), label: "132-Dump site", padding: [.horizontal])
                    MapLegendLine(color: Color(red: 1, green: 0.302, blue: 1), label: "133-Construction sites", padding: [.horizontal])
                    MapLegendLine(color: Color(red: 1, green: 0.651, blue: 1), label: "133-Green Urban areas", padding: [.horizontal])
                    MapLegendLine(color: Color(red: 1, green: 1, blue: 0.659), label: "211-Non-irrigated arable land", padding: [.horizontal])
                    MapLegendLine(color: Color(red: 1, green: 1, blue: 0), label: "212-Permanently irrigated arable land", padding: [.horizontal])
                }
                MapLegendLine(color: Color(red: 0.902, green: 0.902, blue: 0), label: "213-Rice fields", padding: [.horizontal])
                Group {
                    MapLegendLine(color: Color(red: 0.902, green: 0.502, blue: 0), label: "221-Vineyard", padding: [.horizontal])
                    MapLegendLine(color: Color(red: 0.949, green: 0.651, blue: 0.302), label: "222-Fruit trees and berry plantations", padding: [.horizontal])
                    MapLegendLine(color: Color(red: 0.902, green: 0.651, blue: 0), label: "223-Olive groves", padding: [.horizontal])
                    MapLegendLine(color: Color(red: 0.902, green: 0.902, blue: 0.302), label: "231-Pastures", padding: [.horizontal])
                    MapLegendLine(color: Color(red: 0.502, green: 1, blue: 0), label: "311-Broad-leaved forest", padding: [.horizontal])
                    MapLegendLine(color: Color(red: 0, green: 0.651, blue: 0), label: "312-Coniferous forest", padding: [.horizontal])
                    MapLegendLine(color: Color(red: 0.8, green: 0.949, blue: 0.302), label: "321-Natural gasslands", padding: [.horizontal])
                    MapLegendLine(color: Color(red: 0.651, green: 1, blue: 0.502), label: "322-Moors and heathland", padding: [.horizontal])
                    MapLegendLine(color: Color(red: 0.651, green: 0.902, blue: 0.302), label: "323-Sclerophyllous vegetation", padding: [.horizontal])
                    MapLegendLine(color: Color(red: 0.651, green: 0.949, blue: 0), label: "324-Transitional woodland-shrub", padding: [.horizontal])
                }
                MapLegendLine(color: Color(red: 0.902, green: 0.902, blue: 0.902), label: "331-Beaches, dunes, sand", padding: [.horizontal])
                Group {
                    MapLegendLine(color: Color(red: 0.8, green: 0.8, blue: 0.8), label: "332-Bare rocks", padding: [.horizontal])
                    MapLegendLine(color: Color(red: 0.8, green: 1, blue: 0.8), label: "333-Sparsely vegetated areas", padding: [.horizontal])
                    MapLegendLine(color: Color(red: 0, green: 0, blue: 0), label: "334-Burnt areas", padding: [.horizontal])
                    MapLegendLine(color: Color(red: 0.651, green: 0.902, blue: 0.8), label: "335-Glaciers and perpetual snow", padding: [.horizontal])
                    MapLegendLine(color: Color(red: 0.651, green: 0.651, blue: 1), label: "411-Inland wetlands", padding: [.horizontal])
                    MapLegendLine(color: Color(red: 0.8, green: 0.8, blue: 1), label: "421-Maritime wetlands", padding: [.horizontal])
                    MapLegendLine(color: Color(red: 0, green: 0.8, blue: 0.949), label: "511-Water courses", padding: [.horizontal])
                    MapLegendLine(color: Color(red: 0.502, green: 0.949, blue: 0.902), label: "512-Water bodies", padding: [.horizontal])
                    MapLegendLine(color: Color(red: 0, green: 1, blue: 0.651), label: "521-Coastal lagoons", padding: [.horizontal])
                    MapLegendLine(color: Color(red: 0.651, green: 1, blue: 0.902), label: "522-Estuaries", padding: [.horizontal])
                }
                MapLegendLine(color: Color(red: 0.902, green: 0.949, blue: 1), label: "523-Urban fabric", padding: [.horizontal, .bottom])
                
            }
        }
        .background(Color.white)
        //.opacity(0.5)
        }
}
