//
//  GISARDemoApp.swift
//  GISARDemo
//
//  Created by Jakub Jurnik on 15/04/2021.
//

import SwiftUI

@main
struct GISARDemoApp: App {
    
    // MARK: - Life Cycle
        init() {
        }
      
    // MARK: - UI Elements
    @StateObject var arDataModel = ARDataModel()
    var body: some Scene {
        WindowGroup {
            NavigationView {
                MainView().navigationBarTitle("")
                    .navigationBarHidden(true)
            }.navigationViewStyle(StackNavigationViewStyle())
                .environmentObject(arDataModel)
        }
    }
}
