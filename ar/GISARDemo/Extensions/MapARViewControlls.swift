//
//  MapARViewControlls.swift
//  GISARDemo
//
//  Created by Jakub Jurnik on 25.10.2021.
//
import RealityKit
import ARKit
import SwiftUI
import Combine

extension MapARView {
    func setupGestures() {
        print("Gesture Setup")
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        self.addGestureRecognizer(tap)
    }

    func removeMap(){
        let anchor = self.scene.findEntity(named: "mapAnchor")
        if anchor != nil {
            self.scene.removeAnchor(anchor as! HasAnchoring)
        }
        self.mapPlaced = false
    }
    
    func removeArrows(){
        let arrows = self.scene.findEntity(named: "Arrows")
        if arrows != nil {
            arrows?.removeFromParent()
        }
    }
    
    func loadAndPlaceMap(selectedTile: Int) {
            if self.mapFilesDownloaded(selectedTile: selectedTile) {
                self.loadModel(tileNumber: selectedTile)
            } else {
                DispatchQueue.main.async {
                    self.removeArrows()
                    self.downloadTextureFiles(tileNumber: selectedTile)
                    self.downloadModel(tileNumber: selectedTile)
                    self.loadModel(tileNumber: selectedTile)
                }
            }
    }
    
    @objc
    func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        print("screent tapped, selected tile: \(self.selectedTile)")
        if mapPlaced { print("map already in place") }
        else { loadModel(tileNumber: self.selectedTile) }
        guard let touchInView = sender?.location(in: self) else {
            return
          }
          guard let hitEntity = self.entity(
            at: touchInView
          ) else {
            // no entity was hit
            return
          }
        print(hitEntity.name)
        
        switch hitEntity.name {
        case "NorthArrow":
            let destinationTile = neighborrowTileNumber(tileNumber: self.selectedTile, direction: "N")
            self.selectedTile = destinationTile
            print("Destination tile: \(destinationTile)")
            self.loadAndPlaceMap(selectedTile: destinationTile)
            break
        case "SouthArrow":
            let destinationTile = neighborrowTileNumber(tileNumber: self.selectedTile, direction: "S")
            self.selectedTile = destinationTile
            print("Destination tile: \(destinationTile)")
            self.loadAndPlaceMap(selectedTile: destinationTile)
            break
        case "EastArrow":
            let destinationTile = neighborrowTileNumber(tileNumber: self.selectedTile, direction: "E")
            self.selectedTile = destinationTile
            print("Destination tile: \(destinationTile)")
            self.loadAndPlaceMap(selectedTile: destinationTile)
            break
        case "WestArrow":
            let destinationTile = neighborrowTileNumber(tileNumber: self.selectedTile, direction: "W")
            self.selectedTile = destinationTile
            print("Destination tile: \(destinationTile)")
            self.loadAndPlaceMap(selectedTile: destinationTile)
            break
        default:
            break
        }
        
        
        //debug, print all files
        /*
        let documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        do {
            // Get the directory contents urls (including subfolders urls)
            let directoryContents = try FileManager.default.contentsOfDirectory(at: documentsUrl, includingPropertiesForKeys: nil)
            print(directoryContents)

        } catch {
            print(error)
        }
        */
    }
    
    func handleLayerChange(layer: Double = 2000.0, opacity: Double = 0.0) {
        if !mapPlaced {
            //return
        }
        guard let mapModel = try? self.scene.findEntity(named: "mapModel") else {
            print("Error finding model for texture change")
            return
        }
        
        var material = SimpleMaterial()
        material.baseColor = MaterialColorParameter.texture(getMergedTexture(layer: layer, opacity: opacity)!)
        
        var component: ModelComponent = mapModel.components[ModelComponent]!.self
        component.materials = [material]
        mapModel.components.set(component)
    }
    
    func handleTileChange(selectedTile: Int, layer: Double = 1.0, opacity: Double = 1.0) {
        self.selectedTile = selectedTile
        if self.mapPlaced {
            loadAndPlaceMap(selectedTile: selectedTile)
            handleLayerChange(layer: layer, opacity: opacity)
        }
    }
}
