//
//  UIImageMerge.swift
//  GISARDemo
//
//  Created by Jakub Jurnik on 25.10.2021.
//

import SwiftUI

extension UIImage {
    func mergeWith(topImage: UIImage, opacity: CGFloat) -> UIImage {
    let bottomImage = self

    UIGraphicsBeginImageContext(size)

    let areaSize = CGRect(x: 0, y: 0, width: bottomImage.size.width, height: bottomImage.size.height)
    bottomImage.draw(in: areaSize)

        topImage.draw(in: areaSize, blendMode: .normal, alpha: opacity)

    let mergedImage = UIGraphicsGetImageFromCurrentImageContext()!
    UIGraphicsEndImageContext()
    return mergedImage
  }
}
