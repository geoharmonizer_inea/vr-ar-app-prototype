//
//  MapARViewHelpers.swift
//  GISARDemo
//
//  Created by Jakub Jurnik on 25.10.2021.
//

import RealityKit
import ARKit
import SwiftUI
import Combine

extension MapARView {
    func mapFilesDownloaded(selectedTile: Int) -> Bool {
        let fileManager = FileManager.default
        let documentDirectory = try! fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor:nil, create:true)
        let temporaryFileURL = documentDirectory.appendingPathComponent(String(selectedTile)).appendingPathExtension("usdz")
        let filePath = temporaryFileURL.path
        return fileManager.fileExists(atPath: filePath)
    }
    
    func mainViewDownloadedTiles(tiles: [[Int]]) -> [[Bool]] {
        var downloadedArray: [[Bool]] = []
        (0..<10).forEach {vIndex in
            downloadedArray.append([])
            (0..<18).forEach {hIndex in
                if mapFilesDownloaded(selectedTile: tiles[vIndex][hIndex]) {
                    downloadedArray[vIndex].append(true)
                } else {
                    downloadedArray[vIndex].append(false)
                }
            }
        }
        return downloadedArray
    }
    
    func getTextureURL(tileNumber: Int, year: Int) -> URL {
        return URL(string: "https://storage.googleapis.com/cvut_ar_maps_data/textures/lcv_landcover.hcl_lucas.corine.rf_p_30m_0..0cm_\(year)_eumap_epsg3035_v0.1_flip/\(tileNumber).png")!
    }
    
    func getModelURL(tileNumber: Int) -> URL {
        return URL(string: "https://storage.googleapis.com/cvut_ar_maps_data/models/\(tileNumber).usdz")!
    }
    
    func removeDownloadedFiles() {
        print("Deleting saved files")
        let documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!

        do {
            let fileURLs = try FileManager.default.contentsOfDirectory(at: documentsUrl,
                                                                       includingPropertiesForKeys: nil,
                                                                       options: .skipsHiddenFiles)
            for fileURL in fileURLs {
                try FileManager.default.removeItem(at: fileURL)
            }
        } catch  { print(error) }
    }
    
    func removeTileFiles(tileNumber: Int) {
        let documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!

        do {
            let fileURLs = try FileManager.default.contentsOfDirectory(at: documentsUrl,
                                                                       includingPropertiesForKeys: nil,
                                                                       options: .skipsHiddenFiles)
            for fileURL in fileURLs {
                if fileURL.lastPathComponent.contains(String(tileNumber)) {
                    try FileManager.default.removeItem(at: fileURL)
                    //print("deleting file: \(fileURL)")
                }
            }
        } catch  { print(error) }
    }
    
    func neighborrowTileNumber(tileNumber: Int, direction: String) -> Int {
        let currentTileCoordinates = tileArray.indicesOf(x: tileNumber)
        switch direction {
        case "N":
            if currentTileCoordinates![0] > (tileArray.count - 2) {
                return 0
            } else {
                return tileArray[currentTileCoordinates![0]+1][currentTileCoordinates![1]]
            }
        case "S":
            if currentTileCoordinates![0] < 1 {
                return 0
            } else {
                return tileArray[currentTileCoordinates![0]-1][currentTileCoordinates![1]]
            }
        case "W":
            if currentTileCoordinates![1] < 1 {
                return 0
            } else {
                return tileArray[currentTileCoordinates![0]][currentTileCoordinates![1]-1]
            }
        case "E":
            if currentTileCoordinates![1] > (tileArray[0].count - 2) {
                return 0
            } else {
                return tileArray[currentTileCoordinates![0]][currentTileCoordinates![1]+1]
            }
        default:
            return 0
        }
    }
}
