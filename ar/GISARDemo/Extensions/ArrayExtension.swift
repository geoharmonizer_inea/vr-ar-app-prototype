//
//  ArrayExtension.swift
//  GISARDemo
//
//  Created by Jakub Jurnik on 01.12.2021.
//

extension Array where Element : Collection,
    Element.Iterator.Element : Equatable, Element.Index == Int {
    func indicesOf(x: Element.Iterator.Element) -> [Int]? {
        for (i, row) in self.enumerated() {
            if let j = row.firstIndex(of: x) {
                return [i, j]
            }
        }

        return nil
    }
}
