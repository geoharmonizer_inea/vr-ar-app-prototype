//
//  DoubleExtension.swift
//  GISARDemo
//
//  Created by Jakub Jurnik on 25.10.2021.
//

extension Double {
    //dont return decimals
    var clean: String {
       return self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(self)
    }
}
