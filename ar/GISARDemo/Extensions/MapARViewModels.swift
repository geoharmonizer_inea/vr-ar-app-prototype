//
//  MapARViewModels.swift
//  GISARDemo
//
//  Created by Jakub Jurnik on 25.10.2021.
//

import RealityKit
import ARKit
import SwiftUI
import Combine

extension MapARView {
    
    func loadModel(tileNumber: Int) {
        print("LOAD MODEL")
        let fileManager = FileManager.default
        let documentDirectory = try! fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor:nil, create:true)
        let temporaryFileURL = documentDirectory.appendingPathComponent(String(tileNumber)).appendingPathExtension("usdz")
        if self.mapPlaced && self.scene.findEntity(named: "mapModel") != nil{
            var model = self.scene.findEntity(named: "mapModel")
            model = try! Entity.loadModel(contentsOf: temporaryFileURL)
            model!.scale = [0.005, 0.005, 0.005]
            model!.name = "mapModel"
            //addArrowsToModel(parentEntity: self.scene.findEntity(named: "parentEntity")!, tileNumber: tileNumber)
            model!.generateCollisionShapes(recursive: true)
            self.installGestures(.all, for: model as! HasCollision)
        } else {
            let mapAnchor = AnchorEntity(plane: .horizontal)
            mapAnchor.name = "mapAnchor"
            let parentEntity = ModelEntity()
            parentEntity.name = "parentEntity"
            let model = try! Entity.loadModel(contentsOf: temporaryFileURL)
            model.scale = [0.005, 0.005, 0.005]
            model.name = "mapModel"
            parentEntity.addChild(model)
            
            addScaleToModel(parentEntity: parentEntity)
            //addArrowsToModel(parentEntity: parentEntity, tileNumber: tileNumber)
            
            parentEntity.generateCollisionShapes(recursive: true)
            self.installGestures(.all, for: parentEntity as! HasCollision)
            
            mapAnchor.addChild(parentEntity)
            
            self.scene.addAnchor(mapAnchor)
            self.mapPlaced = true
        }
        self.handleLayerChange()
    }
    
    func addScaleToModel(parentEntity: Entity) {
        let scaleXEntity = ModelEntity()
        for index in 1...6 {
            let block = MeshResource.generateBox(width: 0.5/6, height: 0.01, depth: 0.01) // size in metres
            let material = SimpleMaterial(color: index % 2 == 0 ? .white : .black, isMetallic: false)
            let scaleEntity = ModelEntity(mesh: block, materials: [material])
            scaleEntity.position = [(-3.5/12)+Float(index)*(0.5/6),0,0.26]
            scaleXEntity.addChild(scaleEntity)
        }
        scaleXEntity.name = "scaleX"
        let scaleXEntity2 = scaleXEntity.clone(recursive: true)
        scaleXEntity2.position = [0,0,-0.52]
        scaleXEntity2.name = "scaleX2"
        parentEntity.addChild(scaleXEntity)
        parentEntity.addChild(scaleXEntity2)
        
        let scaleYEntity = scaleXEntity.clone(recursive: true)
        scaleYEntity.orientation = simd_quatf(angle: Float.pi/2, axis: [0,1,0])
        scaleYEntity.name = "scaleY"
        let scaleYEntity2 = scaleYEntity.clone(recursive: true)
        scaleYEntity2.position = [-0.52,0,0]
        scaleYEntity2.name = "scaleY2"
        parentEntity.addChild(scaleYEntity)
        parentEntity.addChild(scaleYEntity2)
        
        let text = MeshResource.generateText("30km", extrusionDepth: 0.01, font: .systemFont(ofSize: 0.05), containerFrame: CGRect.zero)
        let textMaterial = SimpleMaterial(color: UIColor.yellow, roughness: 0.0, isMetallic: false)
        let textEntity = ModelEntity(mesh: text, materials: [textMaterial])
        textEntity.name = "scaleDescription"
        textEntity.position = [-0.07,0,0.33]
        textEntity.orientation = simd_quatf(angle: -Float.pi/2, axis: [1,0,0])
        parentEntity.addChild(textEntity)
    }
    
    func addArrowsToModel(parentEntity: Entity, tileNumber: Int){
        let arrowsEntity = ModelEntity()
        arrowsEntity.name = "Arrows"
        let block = MeshResource.generateBox(width: 0.1, height: 0.03, depth: 0.03) // size in metres
        var material = SimpleMaterial(color: .green, isMetallic: false)
        let eArrowEntity = ModelEntity(mesh: block, materials: [material])
        eArrowEntity.position = [0.36,0,0]
        eArrowEntity.name = "EastArrow"
        if neighborrowTileNumber(tileNumber: tileNumber, direction: "E") != 0 {
            arrowsEntity.addChild(eArrowEntity)
        }
        
        material = SimpleMaterial(color: .blue, isMetallic: false)
        let sArrowEntity = ModelEntity(mesh: block, materials: [material])
        sArrowEntity.orientation = simd_quatf(angle: -Float.pi/2, axis: [0,1,0])
        sArrowEntity.position = [0,0,0.38]
        sArrowEntity.name = "SouthArrow"
        if neighborrowTileNumber(tileNumber: tileNumber, direction: "S") != 0 {
            arrowsEntity.addChild(sArrowEntity)
        }
        
        material = SimpleMaterial(color: .red, isMetallic: false)
        let nArrowEntity = ModelEntity(mesh: block, materials: [material])
        nArrowEntity.orientation = simd_quatf(angle: Float.pi/2, axis: [0,1,0])
        nArrowEntity.position = [0,0,-0.38]
        nArrowEntity.name = "NorthArrow"
        if neighborrowTileNumber(tileNumber: tileNumber, direction: "N") != 0 {
            arrowsEntity.addChild(nArrowEntity)
        }
        
        material = SimpleMaterial(color: .yellow, isMetallic: false)
        let wArrowEntity = ModelEntity(mesh: block, materials: [material])
        wArrowEntity.position = [-0.38,0,0]
        wArrowEntity.name = "WestArrow"
        if neighborrowTileNumber(tileNumber: tileNumber, direction: "W") != 0 {
            arrowsEntity.addChild(wArrowEntity)
        }
        parentEntity.addChild(arrowsEntity)
    }
    
    func downloadModel(tileNumber: Int) {
        print("DOWNLOAD MODEL")
        let fileManager = FileManager.default
        let documentDirectory = try! fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor:nil, create:true)
        let temporaryFileURL = documentDirectory.appendingPathComponent(String(tileNumber)).appendingPathExtension("usdz")
        let session = URLSession(configuration: .default, delegate: nil, delegateQueue: nil)
        
        var request = URLRequest(url: getModelURL(tileNumber: tileNumber))
        request.httpMethod = "GET"
        let downloadTask = session.downloadTask(with: request, completionHandler: { (location: URL?,
                                          response: URLResponse?,
                                             error: Error?) -> Void in
                    
                    let fileManager = FileManager.default
                        
                    if fileManager.fileExists(atPath: temporaryFileURL.path) {
                        try! fileManager.removeItem(atPath: temporaryFileURL.path)
                    }
                    try! fileManager.moveItem(atPath: location!.path,
                                              toPath: temporaryFileURL.path)
                })
                downloadTask.resume()
    }
    
    func downloadAndLoadModelFile(tileNumber: Int) {
        let fileManager = FileManager.default
        let documentDirectory = try! fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor:nil, create:true)
        let temporaryFileURL = documentDirectory.appendingPathComponent(String(tileNumber)).appendingPathExtension("usdz")
        let session = URLSession(configuration: .default, delegate: nil, delegateQueue: nil)
        
        var request = URLRequest(url: getModelURL(tileNumber: tileNumber))
        request.httpMethod = "GET"
        let downloadTask = session.downloadTask(with: request, completionHandler: { (location: URL?,
                                          response: URLResponse?,
                                             error: Error?) -> Void in
                    
                    let fileManager = FileManager.default
                        
                    if fileManager.fileExists(atPath: temporaryFileURL.path) {
                        try! fileManager.removeItem(atPath: temporaryFileURL.path)
                    }
                    try! fileManager.moveItem(atPath: location!.path,
                                              toPath: temporaryFileURL.path)
                        
                    DispatchQueue.main.async {
                        do {
                            if self.mapPlaced {
                                var model = self.scene.findEntity(named: "mapModel")
                                model = try Entity.loadModel(contentsOf: temporaryFileURL)
                                model!.scale = [0.005, 0.005, 0.005]
                                model!.name = "mapModel"
                                model!.generateCollisionShapes(recursive: true)
                            } else {
                                let mapAnchor = AnchorEntity(plane: .horizontal)
                                mapAnchor.name = "mapAnchor"
                                let model = try Entity.loadModel(contentsOf: temporaryFileURL)
                                model.scale = [0.005, 0.005, 0.005]
                                model.name = "mapModel"
                                model.generateCollisionShapes(recursive: true)
                                self.installGestures(.all, for: model as! HasCollision)
                                mapAnchor.addChild(model)
                                self.scene.addAnchor(mapAnchor)
                                self.handleLayerChange()
                                self.mapPlaced = true
                            }
                            
                        
                        } catch {
                            print("Fail loading entity.")
                        }
                    }
                })
                downloadTask.resume()
    }
}
