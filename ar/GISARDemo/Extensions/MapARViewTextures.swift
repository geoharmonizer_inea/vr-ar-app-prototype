//
//  MapARViewFiles.swift
//  GISARDemo
//
//  Created by Jakub Jurnik on 25.10.2021.
//
import RealityKit
import ARKit
import SwiftUI
import Combine

extension MapARView {
    
    
    func getMergedTexture(layer: Double, opacity: Double) -> TextureResource? {
        var fileName = ""
        let tileNumber = self.selectedTile
        let fileManager = FileManager.default
        let documentDirectory = try! fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor:nil, create:true)
        
        if opacity == 0 {
            fileName = "\(tileNumber)_\(layer.clean)"
        } else if opacity == 1.0 {
            fileName = "\(tileNumber)_mapLayer"
        } else {
            fileName = "\(tileNumber)_\(layer.clean)_\(opacity)"
        }
        //print(documentDirectory.appendingPathComponent(fileName))
        let texture = try! TextureResource.load(contentsOf: documentDirectory.appendingPathComponent(fileName))
        return texture
    }
    
    
    func downloadTextureFiles(tileNumber: Int) {
        let mapLayerURL = URL(string: "https://storage.googleapis.com/cvut_ar_maps_data/textures/osm_flip/\(tileNumber).png")!
        setTempFile(name: "\(tileNumber)_mapLayer", textureUrl: mapLayerURL)
        
        for year in 2000...2019 {
            let fileName = "\(tileNumber)_\(year)"
            setTempFile(name: fileName, textureUrl: getTextureURL(tileNumber: tileNumber, year: year))
            for opacity in [0.25, 0.5, 0.75] {
                setMergetTexture(textureName: fileName, opacity: opacity, tileNumber: tileNumber)
            }
        }
    }
    
    func setTempFile(name: String, textureUrl: URL) {
        let fileManager = FileManager.default
        let documentDirectory = try! fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor:nil, create:true)
        let temporaryFileURL = documentDirectory.appendingPathComponent(name)
        let textureImageData = NSData(contentsOf: textureUrl)
        let textureImage = UIImage(data: textureImageData! as Data)
        try! textureImage?.pngData()?.write(to: temporaryFileURL)
    }
    
    func setMergetTexture(textureName: String, opacity: Double, tileNumber: Int) {
        let fileManager = FileManager.default
        let documentDirectory = try! fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor:nil, create:true)
        
        let mapImageData = NSData(contentsOf: documentDirectory.appendingPathComponent(textureName))
        let layerImageData = NSData(contentsOf: documentDirectory.appendingPathComponent("\(tileNumber)_mapLayer"))
        
        var mapImage = UIImage(data: mapImageData! as Data)
        let layerImage = UIImage(data: layerImageData! as Data)!
        
        mapImage = mapImage?.mergeWith(topImage: layerImage, opacity: CGFloat(opacity))
        let fileURL = documentDirectory.appendingPathComponent("\(textureName)_\(opacity)")
        try! mapImage?.pngData()?.write(to: fileURL)
    }
}
