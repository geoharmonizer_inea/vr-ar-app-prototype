//
//  MapARViewCoaching.swift
//  GISARDemo
//
//  Created by Jakub Jurnik on 22/04/2021.
//

import ARKit
import UIKit

extension MapARView: ARCoachingOverlayViewDelegate {
  func addCoaching() {
    self.coachingOverlay.delegate = self
    self.coachingOverlay.session = self.session
    self.coachingOverlay.autoresizingMask = [.flexibleWidth, .flexibleHeight]

    // MARK: CoachingGoal
    self.coachingOverlay.goal = .horizontalPlane
    
    self.addSubview(self.coachingOverlay)
  }
  public func coachingOverlayViewDidDeactivate(_ coachingOverlayView: ARCoachingOverlayView) {
    coachingOverlayView.activatesAutomatically = false
  }
}
