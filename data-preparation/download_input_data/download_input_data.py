import os
import requests
from pathlib import Path
from eumap.datasets import Catalogue

def print_filesize(filepath):
    size = filepath.stat().st_size
    if size > 1e6:
        size = int(size / 1e6)
        u = "M"
    else:
        size = int(size / 1e3)
        u = "K"
    print(f"\rFile size: {size}{u}B", end="")

# https://stackoverflow.com/questions/16694907/download-large-file-in-python-with-requests
def download_file(url, local_filename):
    # NOTE the stream=True parameter below
    with requests.get(url, stream=True) as r:
        r.raise_for_status()
        with open(local_filename, 'wb') as f:
            for chunk in r.iter_content(chunk_size=8192): 
                f.write(chunk)
                print_filesize(local_filename)
                
    print_filesize(local_filename)
    print("\n")
    return local_filename

def download(url, filename, target_dir='/data/input/'):
    print(f"Downloading {url}...")
    target_file = Path(target_dir) / filename
    if target_file.exists():
        print(f"File {target_file} already exists. Skipping...\n")
        return
    
    if not target_file.parent.exists():
        os.makedirs(target_file.parent)
    download_file(url, target_file)

if __name__ == "__main__":
    # Tiles
    tiles="adm_tiling.system_30km_m_1m_s0..0m_2020_eumap_epsg3035_v0.1.gpkg"
    download(
        f"https://gitlab.com/geoharmonizer_inea/spatial-layers/-/blob/master/tiling_system/{tiles}",
        filename=f"{tiles}"
    )
    
    # DTM
    dtm="dtm_elev.lowestmode_gedi.eml_mf_30m_0..0cm_2000..2018_eumap_epsg3035_v0.3.tif"
    download(
        f"https://zenodo.org/record/4724549/files/{dtm}?download=1",
        filename=f"dtm/{dtm}"
    )

    # LCV
    cat = Catalogue()
    results = cat.search('land cover', exclude=['corine'])

    for res in results:
        filename = res.split('/')[-1]
        download(res, f"lcv/{filename}")
