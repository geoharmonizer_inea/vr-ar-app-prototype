# Basemap tiles

For the purpose of this project, capability to create basemap tiles
using OpenStreetMap data as input and EPSG:3035 in the output is
required.

Presented solution is based on [MapServer Basemap
project](https://github.com/mapserver/basemaps/) which is modified to
support EPSG:3035.

## Scope

* WMS/WMTS service with the OpenStreetMap data need to be built
* Data is stored in PostGIS database
* Instance will be running in the Docker container

### Download customized software

```sh
git clone https://github.com/opengeolabs/basemaps.git
cd basemaps
git checkout srs_3035
```

### Install shptree requirement

On Debian/Ubuntu OS run:

```sh
sudo apt install mapserver-bin
```

## Prepare and import data

Start docker container:

```sh
docker-compose up -d
```

## Download data

* Pick dataset you want from the https://download.geofabrik.de server
* Use the `pbf` format (not Shapefile)

For our example please download
https://download.geofabrik.de/europe/czech-republic-latest.osm.pbf


In the `basemaps` directory:

```sh
cd data
make
wget https://download.geofabrik.de/europe/czech-republic-latest.osm.pbf
cd ..
```

Import downloaded OSM data to PostGIS database:

```sh
docker-compose exec postgis imposm import -connection postgis://osm@localhost/osm -mapping \
   /app/imposm3-mapping.json -read /app/data/czech-republic-latest.osm.pbf -write -optimize -overwritecache \
   -srid 3857 -deployproduction
```

### Mapfiles

Generate mapfiles:

```sh
docker-compose exec -e OSM_DB_CONNECTION="host=postgis dbname=osm user=osm password=osm port=5432" \
    webserver make
```

# Usage

## shp2img

`shp2img` is MapServer command line utility for generating images
based on input mapfile:

```sh
docker-compose exec webserver shp2img -m /app/osm-default.map \
-o /app/image.png -e 667426 6406816 689883 6423218
```

Generated `image.png` is created in `basemaps` directory.

<!-- TBD: screenshot -->

## WMS

The WMS server is running at localhost on port 80:

http://localhost:80/cgi-bin/mapserv?map=/app/osm-default.map&service=wms&request=getcapabilities

**NOTE:** Since the Capabilies document takes a while to be loaded,
we've prepared cached version, so use this URL when loading into QGIS:
http://localhost:80/osm-default.xml

# Tuning

1. You may want to change the database password in the `docker-compose.yml` file
```
    environment:
      - POSTGRES_PASSWORD=vr_basemaps
```
2. You may want to change the port for the webserver in `docker-compose.yml`
```
    ports:
        - "80:80"
```
3. You may want to generate different style map, use
```sh
docker-compose exec -e OSM_DB_CONNECTION="host=postgis dbname=osm user=osm password=osm port=5432" webserver make STYLE=google
```
