#!/usr/bin/env python3

import os
import time
import shutil
import argparse
from glob import glob
from pathlib import Path

from osgeo import ogr

class TileFid:
    def __init__(self, path):
        self.ds = ogr.Open(path, True)
        self.layer = self.ds.GetLayer()

    def __del__(self):
        self.ds = None
        
    def hasFid(self):
        layer_def = self.layer.GetLayerDefn()

        for i in range(layer_def.GetFieldCount()):
            if layer_def.GetFieldDefn(i).GetName() == "tile_id":
                return True

        return False

    def addFid(self, field_name="tile_id"):
        field = ogr.FieldDefn(field_name, ogr.OFTInteger)
        self.layer.CreateField(field)

        feat = self.layer.GetNextFeature()        
        while feat:
            feat.SetField(field_name, feat.GetFID())
            self.layer.SetFeature(feat)
            feat = self.layer.GetNextFeature()

def main(input_dir, tiles, output_dir, output_format, output_type):
    color_table = '/opt/lcv_color_table.txt' if output_type == "Byte" else None
    for tif in glob(input_dir + os.path.sep + '*.tif'):
        process_tif(tif, tiles, Path(output_dir) / Path(tif).stem, output_format, output_type, color_table)

def process_tif(input_tif, tiles, output_dir, output_format, output_type, color_table=None):
    from grass.pygrass.modules import Module
    from grass.pygrass.vector import VectorTopo
    from grass.exceptions import CalledModuleError

    if os.path.exists(output_dir):
        shutil.rmtree(output_dir)
    os.makedirs(output_dir)

    # pygrass bug
    # Module("v.in.ogr", input=tiles, output="tiles", key="fid")
    tf = TileFid(tiles)
    if not tf.hasFid():
        tf.addFid()

    Module("v.in.ogr", input=tiles, output="tiles")
    input_map = Path(input_tif).stem
    Module("r.external", input=input_tif, output=input_map, flags='o')
    if color_table: 
        Module('r.colors', map=input_map, rules=color_table)

    # n <-> yu
    # s <-> yl
    # w <-> xl
    # e <-> xu

    if output_format == 'GTiff':
        extension = '.tif'
    elif output_format == 'PNG':
        extension = '.png'
    else:
        raise Exception("Unsupported format")

    start = time.time()
    with VectorTopo("tiles") as v:
        count = v.number_of('areas')
        i = 1
        for t in v.viter('areas'):
            print(f"Processing {i} of {count}")
            Module('g.region', n=t.attrs['yu'], s=t.attrs['yl'], w=t.attrs['xl'], e=t.attrs['xu'], align=input_map)
            try:
                Module('r.out.gdal', flags="" if color_table else "c", input=input_map,
                       output=os.path.join(output_dir, str(t.attrs['tile_id']) + extension),
                       type=output_type, format=output_format)
            except CalledModuleError:
                # https://gitlab.com/ctu-geoforall-lab/ctu-geoharmonizer/-/issues/134
                pass
            i += 1

    print("Time elapsed: {}sec".format(time.time() - start))

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--input', metavar='input_dir', type=str,
                        help='Input directory containing input TIF files')
    parser.add_argument('--tiles', metavar='tiles_gpkg', type=str,
                        help='Input GPKG file containing tiles')
    parser.add_argument('--output', metavar='output_dir', type=str,
                        help='Output directory where tiles will be stored')
    # parser.add_argument('--format', metavar='format', type=str,
    #                     help='Data format for output files (eg. GTiff or PNG)')    
    parser.add_argument('--datatype', metavar='datatype', type=str,
                        help='Data type for output files (eg. Byte or Int16)')

    args = parser.parse_args()

    os.environ['GRASS_OVERWRITE'] = '1'
    main(os.path.expanduser(args.input), args.tiles, os.path.expanduser(args.output), "GTiff", args.datatype)
