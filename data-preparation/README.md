# Data Preparation

Requirements: 

* [Docker](https://docker.com)
* [QGIS](https://qgis.org)

## 1. Build docker images

Build three docker images:

* `download_input_data`: to download input data
* `make_img_tiles`: to generate for each tile DTM and Land Cover image file
* `make_map_tiles`: to generate basemap images
* `flip_tiles`: to flip tiles vertically for VR purpose

```sh
docker build -t download_input_data data-preparation/download_input_data/
docker build -t make_img_tiles data-preparation/make_img_tiles/
docker build -t make_map_tiles data-preparation/make_map_tiles/
docker build -t flip_tiles data-preparation/flip_tiles/
```

## 2. Download input data 

* Tiling system: [adm_tiling.system_30km_m_1m_s0..0m_2020_eumap_epsg3035_v0.1.gpkg](https://gitlab.com/geoharmonizer_inea/spatial-layers/-/blob/master/tiling_system/adm_tiling.system_30km_m_1m_s0..0m_2020_eumap_epsg3035_v0.1.gpkg)
* [Continental Europe Digital Terrain Model at 30 m resolution](https://zenodo.org/record/4724549#.Ya9R8JHMLd4)
* [Multi-year Harmonized Land Cover Dataset](https://data.opendatascience.eu/geonetwork/srv/eng/catalog.search#/metadata/ef733487-057f-440a-9d55-e4eac13f6fdc)

Input data can be automatically downloaded by running docker container
`download_input_data`. In example below a local directory
`/data/geoharmonizer/vr` will be used.

By running command below a new directory
`/data/geoharmonizer/vr/input` will created. This directory contains
downloaded input data:

```sh
docker run --rm -v /data/geoharmonizer/vr/:/data download_input_data
```

Note: Download process can take a while since large datasets are
processed. Overall size of downloaded data is more than 40 GB!

Expected data layout of `input` directory:

```sh
├── input
    ├── adm_tiling.system_30km_m_1m_s0..0m_2020_eumap_epsg3035_v0.1.gpkg
    ├── dtm
    │   └── dtm_elev.lowestmode_gedi.eml_mf_30m_0..0cm_2000..2018_eumap_epsg3035_v0.3.tif
    ├── lcv
    │   ├── lcv_landcover.hcl_lucas.corine.rf_p_30m_0..0cm_2000_eumap_epsg3035_v0.1.tif
    │   ├── lcv_landcover.hcl_lucas.corine.rf_p_30m_0..0cm_2001_eumap_epsg3035_v0.1.tif
    │   ├── lcv_landcover.hcl_lucas.corine.rf_p_30m_0..0cm_2002_eumap_epsg3035_v0.1.tif
    ...
```

### Select tiles to be processed

Open vector layer
`adm_tiling.system_30km_m_1m_s0..0m_2020_eumap_epsg3035_v0.1.gpkg`
with tiling system in QGIS and select tiles to be processed.

Workflow for countries:

1. Type 'world' in statusbar (coordinates) to load a world map in QGIS
2. Select specified country
3. Select tiles by `Vector -> Research Tools -> Select by location` tool

<img src="img/qgis_select_by_location.png"/><br/>
<i>Select tiles to be processed in QGIS.</i>

Save selected tiles as a new Geopackage file placed in the `input`
directory. In our example as `tiles_cz.gpkg` file.

```sh
├── input
    ├── adm_tiling.system_30km_m_1m_s0..0m_2020_eumap_epsg3035_v0.1.gpkg
    ├── dtm
    ...
    ├── lcv
    ├── tiles_cz.gpkg
```

## 3. Generate DTM and LCV tiles 

Input data directory specified by `--input` parameter is processed
based on tiles defined by parameter `--tiles`. Docker container
`make_img_tiles` writes generated tiles (each tile in separated
image file) into target directory which is defined by `--output`
parameter.

### Process DTM

```sh
docker run --rm -v /data/geoharmonizer/vr/:/data \
   make_img_tiles \
   grass -c EPSG:3035 /tmp/location --exec python3 /opt/make_img_tiles.py \
   --input /data/input/dtm/ \
   --tiles /data/input/tiles_cz.gpkg \
   --output /data/tiles_cz \
   --datatype Int16
```

### Process Land Cover

```sh
docker run --rm -v /data/geoharmonizer/vr/:/data \
   make_img_tiles \
   grass -c EPSG:3035 /tmp/location --exec python3 /opt/make_img_tiles.py \
   --input /data/input/lcv \
   --tiles /data/input/tiles_cz.gpkg \
   --output /data/tiles_cz \
   --datatype Byte
```

Generated tiles in GeoTIFF format are stored in directory named by
input data layer.

Example of output directory `tiles_cz`:

```sh
├── tiles_cz
    ├── dtm_elev.lowestmode_gedi.eml_mf_30m_0..0cm_2000..2018_eumap_epsg3035_v0.3
    |   ├── 12155.tif
    |   ├── 12156.tif
    |   ├── 12157.tif
    |    ...
    ├── lcv_landcover.hcl_lucas.corine.rf_p_30m_0..0cm_2000_eumap_epsg3035_v0.1
    |   ├── 12155.tif
    |   ├── 12156.tif
    |   ├── 12157.tif
    |    ...
    ├── lcv_landcover.hcl_lucas.corine.rf_p_30m_0..0cm_2001_eumap_epsg3035_v0.1
    |   ├── 12155.tif
    |   ├── 12156.tif
    │   ├── 12157.tif
    |    ...
    ├── lcv_landcover.hcl_lucas.corine.rf_p_30m_0..0cm_2002_eumap_epsg3035_v0.1
    │   ├── 12155.tif
    │   ├── 12156.tif
    │   ├── 12157.tif
    |   ...
```

## 4. Generate Basemap (OSM) tiles

### Deploying base maps service

Deploy [WMS service based on OpenStreetMap data](basemap.md).

Fix `osm-default.map` file manually: The default projection (see `PROJECTION`
section at the beginning of the file) is `EPSG:3857`. Our project relies on
ETRS89 (EPSG:3035) - you have to open the file in your favourite `$EDITOR` and
change the default projection. Than you can continue

### Generate tile images

```sh
docker run --rm -v /data/geoharmonizer/vr/:/data map_images \
    python3 /opt/make_map_tiles.py \
    /data/input/tiles_cz.gpkg \
    osm \
    --size 2048
```

Default image size is 800×800 px, it can be adjusted by the `--size`
parameter.

As `input`, you may provide input vector file (EPSG:3035) containing
selected tiles. The `output` directory will be created in the
`/data/geoharmonizer/vr/tiles_cz`:

```sh
├── tiles_cz
    ├── dtm_elev.lowestmode_gedi.eml_mf_30m_0..0cm_2000..2018_eumap_epsg3035_v0.3
    |   ...
    ├── lcv_landcover.hcl_lucas.corine.rf_p_30m_0..0cm_2000_eumap_epsg3035_v0.1
    |   ...
    ├── lcv_landcover.hcl_lucas.corine.rf_p_30m_0..0cm_2001_eumap_epsg3035_v0.1
    |    ...
    ├── lcv_landcover.hcl_lucas.corine.rf_p_30m_0..0cm_2002_eumap_epsg3035_v0.1
    |   ...
    ├── osm
    │   ├── 12155.png
    │   ├── 12156.png
    │   ├── 12157.png
    |   ...
```

## 5. Flip tiles

Tested models have be flipped UV so texture is loaded vertically
flipped. This is a reason why generated LCV and OSM tiles have to be
also flipped.

```sh
docker run --rm -v /data/geoharmonizer/vr/:/data flip_tiles \
   python3 /opt/flip_tiles.py \
   --input /data/tiles_cz \
   --output /data/tiles_cz_vr
```

```sh
├── tiles_cz_vr
    ├── lcv_landcover.hcl_lucas.corine.rf_p_30m_0..0cm_2000_eumap_epsg3035_v0.1
    |   ├── 12155.png
    |   ├── 12156.png
    |   ├── 12157.png
    |    ...
    ├── lcv_landcover.hcl_lucas.corine.rf_p_30m_0..0cm_2001_eumap_epsg3035_v0.1
    |   ├── 12155.png
    |   ├── 12156.png
    │   ├── 12157.png
    |    ...
    ├── lcv_landcover.hcl_lucas.corine.rf_p_30m_0..0cm_2002_eumap_epsg3035_v0.1
    │   ├── 12155.png
    │   ├── 12156.png
    │   ├── 12157.png
    |   ...
    ├── osm
    │   ├── 12155.png
    │   ├── 12156.png
    │   ├── 12157.png
    |   ...
```

## 6. Convert DTM tiles to glTF

DTM tiles (see step 3) have to be converted from GeoTIFF to a
VR-friendly glTF format. The procedure is based on
[QGIS](https://qgis.org/) and [Qgis2Threejs
plugin](https://plugins.qgis.org/plugins/Qgis2threejs/).

### Change QGIS settings

We are using QGIS Macros for performing the code. If you want to run
the macro automatically after QGIS starts, you need to go to `Settings
-> Options -> General` and set `Enable macros` option (at the bottom
of the `General` panel) to `Always (not recommended)`.

### Perform GeoTIFF to glTF conversion

Input DTM GeoTIFF files are expected in
`tiles_cz/dtm_elev.lowestmode_gedi.eml_mf_30m_0..0cm_2000..2018_eumap_epsg3035_v0.3`
directory.

1. Set `glTF_INPUT` environment variable pointing to directory with
   input GeoTIFF files
   
   ```bash
   export GLTF_INPUT=/data/geoharmonizer/vr/dtm_elev.lowestmode_gedi.eml_mf_30m_0..0cm_2000..2018_eumap_epsg3035_v0.3
   ```
   
   Optionally also output directory for glTF files can be defined. If
   not defined than glTF files are generated in temporary directory
   
   ```bash
   export GLTF_OUTPUT=/data/geoharmonizer/vr/dtm_elev.lowestmode_gedi.eml_mf_30m_0..0cm_2000..2018_eumap_epsg3035_v0.3_gltf
   ```
   
   DMT size can be set by `GLTF_DEM_SIZE` environment variable.
   
   ```bash
   export GLTF_DEM_SIZE=3
   ```
   
   Default `DEM_SIZE` value is 6.

2. Run QGIS with the `gltf_project.qgz` project. The project is empty, but
   contains the macro configuration (see `Project -> Properties -> Macros`).

   ```bash
   qgis --project threejs/gltf_project.qgz
   ```
   
   <img src="img/qgis_progress.png"/><br/>
   <i>Progress of computation is reported by QGIS.</i>
   
Generated glTF files will be stored in output directory (QGIS will
show the location after calculation is done).

Output glTF files are generated in
`tiles_cz_vr/dtm_elev.lowestmode_gedi.eml_mf_30m_0..0cm_2000..2018_eumap_epsg3035_v0.3`:

```sh
├── tiles_cz_vr
    ├── dtm_elev.lowestmode_gedi.eml_mf_30m_0..0cm_2000..2018_eumap_epsg3035_v0.3
    |   ├── 12155.gltf
    |   ├── 12156.gltf
    |   ├── 12157.gltf
    |   ...
    ├── lcv_landcover.hcl_lucas.corine.rf_p_30m_0..0cm_2000_eumap_epsg3035_v0.1
    |   ...
    ├── lcv_landcover.hcl_lucas.corine.rf_p_30m_0..0cm_2001_eumap_epsg3035_v0.1
    |   ...
    ├── lcv_landcover.hcl_lucas.corine.rf_p_30m_0..0cm_2002_eumap_epsg3035_v0.1
    |   ...
    ├── osm
    |   ...
```

Note: For viewing of the glTF data, we found the online viewer
https://playcanvas.com/viewer useful.

## Result of data preparation

Expected data layout of `tiles_cz_vr` directory:

```sh
├── tiles_cz_vr
    ├── dtm_elev.lowestmode_gedi.eml_mf_30m_0..0cm_2000..2018_eumap_epsg3035_v0.3
    |   ├── 12155.gltf
    |   ├── 12156.gltf
    |   ├── 12157.gltf
    |   ...
    ├── lcv_landcover.hcl_lucas.corine.rf_p_30m_0..0cm_2000_eumap_epsg3035_v0.1
    |   ├── 12155.png
    |   ├── 12156.png
    |   ├── 12157.png
    |    ...
    ├── lcv_landcover.hcl_lucas.corine.rf_p_30m_0..0cm_2001_eumap_epsg3035_v0.1
    |   ├── 12155.png
    |   ├── 12156.png
    │   ├── 12157.png
    |    ...
    ├── lcv_landcover.hcl_lucas.corine.rf_p_30m_0..0cm_2002_eumap_epsg3035_v0.1
    │   ├── 12155.png
    │   ├── 12156.png
    │   ├── 12157.png
    |   ...
    ├── osm
    │   ├── 12155.png
    │   ├── 12156.png
    │   ├── 12157.png
    |   ...
```
