import argparse
import os
import sys
import tempfile
import glob
from string import Template

from PyQt5.QtCore import QSize, Qt
from PyQt5.QtNetwork import QNetworkDiskCache
from Qgis2threejs.export import ThreeJSExporter, ImageExporter, ModelExporter
import Qgis2threejs

from Qgis2threejs.mapextent import MapExtent
from qgis.core import QgsProject
from qgis.testing import start_app
from qgis.gui import QgsMapCanvas, QgsLayerTreeMapCanvasBridge
from qgis.core import QgsRasterLayer
from qgis.utils import iface, Qgis
import time

def export_gltfs(input_dir: str, output_dir: str):
    """Export multiple TIFF files into GLTF format.
    """
    if output_dir is None:
        output_dir = tempfile.mkdtemp(prefix="gltf-output-")
    else:
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)

    # collect TIF files
    files = glob.glob(os.path.join(input_dir, "*.tif"))
    fcount = len(files)
    i = 1
    for f in files:
        iface.messageBar().pushMessage(
            "GLTF export",
            "Processing {} ({} out of {})".format(f, i, fcount),
            level=Qgis.Info,
            duration=1
        )
        export_gltf(os.path.join(input_dir, f), output_dir)
        i += 1

    iface.messageBar().clearWidgets()

    return output_dir

def export_gltf(tiff: str, output_dir: str):
    """Convert single TIF file to GLTF.

    :param str tiff: path to input TIF file
    :param str output_dir: path to output directory
    """
    basename = os.path.basename(tiff)
    # texture base size
    TEX_WIDTH, TEX_HEIGHT = (1024, 1024)

    # get map settings from current map canvas
    project = QgsProject.instance()
    root = project.layerTreeRoot()
    mapCanvas = QgsMapCanvas()
    bridge = QgsLayerTreeMapCanvasBridge(root, mapCanvas)

    raster = QgsRasterLayer(tiff, "raster")
    gltf_file=os.path.join(output_dir, basename.replace(".tif", ".gltf"))
    project.addMapLayer(raster)
    rext = raster.extent()
    mapCanvas.setExtent(rext)
    mapSettings = mapCanvas.mapSettings()
    center = mapSettings.extent().center()
    raster.triggerRepaint()
    mapCanvas.refresh()

    settings_file = os.path.join(
        output_dir,
        "{}.qto3settings".format(basename.replace(".tif", ""))
    )

    with open(os.path.join(os.path.dirname(__file__), "settings.qto3settings")) as ms:
        with open(settings_file, "w") as out:
            template = Template(ms.read())
            out.write(template.substitute(
                raster=raster.id(),
                x=center.x(),
                y=center.y(),
                raster_name=basename,
                dem_size=os.environ.get("GLTF_DEM_SIZE", "6")
            ))


    # extent to export
    width = mapSettings.extent().width()
    height = width * TEX_HEIGHT / TEX_WIDTH
    rotation = 0

    # apply the above extent to map settings
    MapExtent(center, width, height, rotation).toMapSettings(mapSettings)

    # texture base size
    mapSettings.setOutputSize(QSize(TEX_WIDTH, TEX_HEIGHT))

    exporter = ModelExporter()
    exporter.loadSettings(settings_file)
    exporter.setMapSettings(mapSettings)

    exporter.initWebPage(1024, 768)
    exporter.export(gltf_file)
    time.sleep(2)
    project.clear()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Export scene to glTF')
    parser.add_argument('--input', metavar='FILE', type=str, required=True,
                        help='Input Raster file (tiff)')
    parser.add_argument('--output', metavar='DIR', type=str,
                        help='Output directory')

    args = parser.parse_args()

    export_gltf(args.input, args.output)
