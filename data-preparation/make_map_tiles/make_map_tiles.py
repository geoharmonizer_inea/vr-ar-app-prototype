#!/usr/bin/env python3

#
# Script for generating map images according to required extent and scale
#
# author: jachym.cepicky at opengeolabs dot cz
# 

import fiona
import argparse
import docker
import _io
import os
from shapely.geometry import shape

class GeneratorError(Exception):
    pass

def get_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(description='Process input vector data tiles')
    parser.add_argument(
        'input_file', metavar='GPKG',
        type=argparse.FileType('r'),
        help='Input file name, usually GPKG'
    )
    parser.add_argument(
        'output_dir', metavar='DIR',
        type=str,
        default='output',
        help='Output directory'
    )
    parser.add_argument(
            '--size', metavar='INT', type=int,
            default=800,
            help="Change output image size (default is 800)"
    )

    return parser.parse_args()

def get_container(client:docker.client.DockerClient) -> docker.models.containers.Container:
    for c in client.containers.list():
        if c.image.tags and c.image.tags[0].find('basemaps_webserver') > -1:
            return c
    raise GeneratorError("""Could not find basemaps_webserver docker container.
    Did you start docker-compose up in the mapserver/basemaps project?""")

def generate_image(
        container: docker.models.containers.Container,
        feature: dict,
        output: str,
        size: int=800):
    geom = shape(feature["geometry"])
    extent = geom.bounds
    if "tile_id" in feature["properties"]:
        tile_id = feature["properties"]["tile_id"]
    else:
        tile_id = feature["id"]
    cmd = "shp2img -m /app/osm-default.map -s {size} {size} -o /app/{output}/{file}.png -e {extent[0]} {extent[1]} {extent[2]} {extent[3]}".format( extent=extent, output=output,
    file=tile_id, size=size)
    (code, output) = container.exec_run(cmd.split())
    return code, output


def generate_images(inpt: _io._IOBase, output: str, size: int):

    client = docker.from_env()
    container = get_container(client)
    print("Creating output directory in /app/output")
    (code, out) = container.exec_run('mkdir /app/{output}'.format(output=output))
    with fiona.open(inpt.name, "r") as data:
        for f in data:
            if "tile_id" in f["properties"]:
                tile_id = f["properties"]["tile_id"]
            else:
                tile_id = f["id"]
            code, out = generate_image(container, f, output, size)
            print(tile_id, code, out)

def main():
    args = get_args()

    if not os.path.isdir(args.output_dir):
        os.mkdir(args.output_dir)

    generate_images(args.input_file, args.output_dir, args.size)

if __name__ == "__main__":
    main()
