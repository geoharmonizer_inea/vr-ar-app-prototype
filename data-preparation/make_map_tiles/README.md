# Generate tile images

Make sure, `docker-compose up` of the basemap project is up and running, see
[basemape.md](../vr/basemap.md) for details

Fix `osm-default.map` file manually: The default projection (see `PROJECTION`
section at the beginning of the file) is `EPSG:3857`. Our project relies on
ETRS89 (EPSG:3035) - you have to open the file in your favourite `$EDITOR` and
change the default projection. Than you can continue

## Generate images

Make sure, `requirements.txt` are installed, make sure, `docker-compose up` is
running

```
python3 make_map_tiles.py cz_tiles-mercator.gpkg output --size 2048
```

Default image size is 800×800 px, it can be adjusted by the `--size` parameter.

as `input`, you may provide intput vector file (EPSG:3857). The `output`
directory will be created in the `/app/` directory fo the `webserver` container
(so probably somewhere in the `basemaps` dir).
