#!/usr/bin/env python3

import os
import argparse
from glob import glob
import shutil
import subprocess
from pathlib import Path

from flip_img import flip_imgs

def main(input_dir, output_dir):
    dirs = [ os.path.join(input_dir, name) for name in os.listdir(input_dir)
             if os.path.isdir(os.path.join(input_dir, name)) and not name.startswith('dtm') ]
    dcount = len(dirs)
    i = 1
    for idir in dirs:
        odir = Path(output_dir) / idir
        print("Processing dir {} out of {} ({})...".format(i, dcount, Path(idir).name))
        flip_imgs(idir, odir)
        i += 1
    
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--input', metavar='input_dir', type=str, help='Input data directory')
    parser.add_argument('--output', metavar='output_dir', type=str, help='Output data directory')

    args = parser.parse_args()

    main(args.input, args.output)
