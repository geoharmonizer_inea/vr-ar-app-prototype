#!/usr/bin/env python3

import os
import argparse
from glob import glob
import shutil
import subprocess

def flip_imgs(input_dir, output_dir):
    if os.path.exists(output_dir):
        shutil.rmtree(output_dir)
    os.makedirs(output_dir)

    for ext in ('tif', 'png'):
        for tif in glob(input_dir + os.path.sep + '*.{}'.format(ext)):
            args = [ "convert", tif, "-flip" ]
            if os.path.basename(input_dir).startswith("osm"):
                args += [ "-background", "#b3c6d4", "-alpha", "remove", "-alpha", "off" ]
            args += [ os.path.join(output_dir, os.path.splitext(os.path.basename(tif))[0] + '.png') ]
            subprocess.run(args, stderr=subprocess.DEVNULL)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('input', metavar='input_dir', type=str, help='Input directory containing input files')
    parser.add_argument('output', metavar='output_dir', type=str, help='Output directory where output files will be stored')

    args = parser.parse_args()

    flip_imgs(os.path.expanduser(args.input), os.path.expanduser(args.output))
